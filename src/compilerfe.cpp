#include <vector>
#include "compilerfe.h"
#include "outpipe.h"
#include "utils.h"

using namespace win32fe;

compiler::compiler() {
  compileoutflag = linkoutflag = "-o ";

  OutputFlag = compilearg.end();

  Options['D'] = &compiler::FoundD;
  Options['I'] = &compiler::FoundI;
  Options['L'] = &compiler::FoundL;
  Options['O'] = &compiler::FoundO;
  Options['c'] = &compiler::Foundc;
  Options['g'] = &compiler::Foundg;
  Options['l'] = &compiler::Foundl;
  Options['o'] = &compiler::Foundo;
  Options['m'] = &compiler::Foundm;

  doit = &compiler::Link;
}

int compiler::Parse(void) {
  int ierr = tool::Parse();
  if (!ierr) {
    LI i = arg.begin();
    compilearg.push_front(*i++);
    arg.pop_front();
    while (i != arg.end()) {
      std::string temp = *i;
      if (temp[0]!='-') {
        FoundFile(i);
      } else {
        char flag = temp[1];
        std::map<char,compiler::ptm_parse>::iterator it = this->Options.find(flag);
        if (it == this->Options.end()) {
          FoundUnknown(i);
        } else {
          (this->*it->second)(i);
        }
      }
      i++;
      arg.pop_front();
    }
  } else {
    return(ierr);
  }
  return(0);
}

int compiler::Execute(void) {
  int ierr = tool::Execute();
  if (!ierr && compiler::doit) {
    FixOutput();
    ierr = (this->*doit)();
  }
  return(ierr);
}

int compiler::Help(void) {
  tool::Help();
  std::cout << "For compilers:" << std::endl;
  std::cout << "  win32fe will map the following <tool options> to their native options:" << std::endl;
  std::cout << "    -c:          Compile Only, generates an object file with .o extension" << std::endl;
  std::cout << "                 This will invoke the compiler once for each file listed." << std::endl;
  std::cout << "    -l<library>: Link the file lib<library>.lib or if using --win-l also, <library>.lib" << std::endl;
  std::cout << "    -o <file>:   Output=<file> context dependent" << std::endl;
  std::cout << "    -D<macro>:   Define <macro>" << std::endl;
  std::cout << "    -I<path>:    Add <path> to the include path" << std::endl;
  std::cout << "    -L<path>:    Add <path> to the link path" << std::endl;
  std::cout << "    -g:          Generate debug symbols in objects when specified for compilation," << std::endl;
  std::cout << "                 and in executables when specified for linking (some compilers" << std::endl;
  std::cout << "                 specification at both times for full debugging support)." << std::endl;
  std::cout << "    -O:          Enable compiletime and/or linktime optimizations." << std::endl;
  std::cout << "Ex: win32fe cl -g -c foo.c --verbose -Iinclude" << std::endl << std::endl;
  std::cout << "Note: win32fe will automatically find the system library paths and" << std::endl;
  std::cout << "      system include paths, relieving the user of the need to invoke a" << std::endl;
  std::cout << "      particular shell." << std::endl << std::endl;
  std::cout << "=========================================================================" << std::endl << std::endl;
  return(0);
}

int compiler::Compile(void) {
  int n,ierr=0;
  LI i = compilearg.begin();
  std::string compile = *i++;
  win32fe::Merge(compile,std::string::npos,compilearg,i);

  /* Execute each compilation one at a time */ 
  for (i=file.begin();i!=file.end();i++) {
    std::string outfile = *i;

    if (OutputFlag==compilearg.end()) {
      /* Make default output a .o not a .obj */
      n = outfile.rfind(".");
      outfile = compileoutflag + outfile.substr(0,n) + ".o";
    } else {
      /* remove output file from compilearg list */
      outfile = *OutputFlag;
      compilearg.erase(OutputFlag);
      OutputFlag = compilearg.end();
      LI ii = compilearg.begin();
      compile = *ii++;
      win32fe::Merge(compile,std::string::npos,compilearg,ii);
    }

    /* Check existance of filename, but use natural form. */
    std::string filename = *i;
    if (!win32fe::GetShortPath(filename)) {
      std::cout << "Error: win32fe: Input File Not Found: ";
      std::cout << *i << std::endl;
      return(-1);
    }
    
    std::string compileeach = compile + " " + outfile + " " + *i;
    ierr = Report(compileeach.c_str());
    if (ierr) break;
  }
  return(ierr);
}

int compiler::Link(void) {
  /* Copy all <file>.o in file list to <file>.obj */ 
  int i,ierr=0;
  LI f;
  /* Change these to list<string> someday */
  std::vector<std::string> ext(file.size(),""); /* File extensions */
  std::vector<std::string> temp(file.size(),"");

  for (i=0,f=file.begin();f!=file.end();i++,f++) {
    size_t len = (*f).length();
    std::string::size_type n = (*f).rfind(".");
    std::string outfile = (*f).substr(0,n) + ".obj";
    if (n < len) {
      ext[i] = (*f).substr(n,std::string::npos);
    } else {
      ext[i] = "";
    }
    if (ext[i] == ".o") {
      temp[i]=*f;
      std::string copy = "copy " + temp[i] + " " + outfile;
      if (verbose) {
        std::cout << copy << std::endl;
      }
      win32fe::CopyFile(temp[i],outfile);
      /* Replace <file>.o in the file list with <file>.obj */
      f = file.erase(f);
      f = file.insert(f,outfile);
    }
  }

  std::string link = BuildLinkCommand();
  ierr = Report(link.c_str());

  /* Remove file.obj's */
  for (i=0,f=file.begin();f!=file.end();i++,f++) {
    if (ext[i] == ".o") {
      win32fe::DeleteFile(*f,verbose);
      f = file.erase(f);
      f = file.insert(f,temp[i]);
    }
  }
  return(ierr);
}

std::string compiler::BuildLinkCommand(void) {
  LI i = compilearg.begin();
  std::string link = *i++;
  win32fe::Merge(link,std::string::npos,compilearg,i);
  i = file.begin();
  win32fe::Merge(link,std::string::npos,file,i);
  i = linkarg.begin();
  win32fe::Merge(link,std::string::npos,linkarg,i);
  return(link);
}

void compiler::FoundD(LI &i) {
  std::string temp = *i;
  win32fe::ProtectQuotes(temp);
  compilearg.push_back(temp);
  return;
}

void compiler::FoundI(LI &i) {
  std::string shortpath = i->substr(2);
  if (win32fe::GetShortPath(shortpath)) {
    shortpath = "-I"+shortpath;
    compilearg.push_back(shortpath);
  } else {
    if (!woff) {
      std::cout << "Warning: win32fe: Include Path Not Found: " << i->substr(2) << std::endl;
    }
  }
  return;
}

void compiler::FoundL(LI &i) {
  std::string shortpath =i->substr(2);
  if (win32fe::GetShortPath(shortpath)) {
    shortpath = "-L"+shortpath;
    linkarg.push_back(shortpath);
  } else {
    if (!woff) {
      std::cout << "Warning: win32fe: Library Path Not Found: " << i->substr(2) << std::endl;
    }
  }
  return;
}

void compiler::FoundO(LI &i) {
  compilearg.push_back(*i);
  return;
}

void compiler::Foundc(LI &i) {
  std::string temp = *i;
  if (temp=="-c") {
    compiler::doit = &compiler::Compile;
  }
  compilearg.push_back(temp);
  return;
}

void compiler::Foundg(LI &i) {
  compilearg.push_back(*i);
  return;
}

void compiler::Foundl(LI &i) { 
  std::string temp = *i;
  file.push_back(tool::libprefix + temp.substr(2) + ".lib");
  return;
} 

void compiler::Foundo(LI &i) {
  if (*i == "-o") {
    i++;
    arg.pop_front();
    std::string temp = *i;
    /* Be sure output directory exists. */
    std::string outdir;
    std::string::size_type n = temp.rfind("\\");
    if (n==std::string::npos) {
      n = temp.rfind("/");
    }
    if (n==std::string::npos) {
      outdir = win32fe::GetCurrentDirectory() + "\\";
    } else {
      n++;
      outdir  = temp.substr(0,n);
      temp = temp.substr(n);
    }
    if (!win32fe::GetShortPath(outdir)) {
      std::cout << "Warning: win32fe: Output directory not found: " << outdir << std::endl;
    }
    if (outdir.rfind("\\")!=outdir.length()-1) {
      outdir += "\\";
    }
    temp = outdir + temp;
    compilearg.push_back(temp);
    /* Set Flag then fix later based on compilation or link */
    OutputFlag = --compilearg.end();
  } else {
    compilearg.push_back(*i);
  }
  return;
}   

void compiler::Foundm(LI &i) {
  std::string temp = *i;
  std::cout << "Warning: win32fe: ignoring option: " << temp << std::endl;
  return;
}

void compiler::FoundUnknown(LI &i) {
  std::string temp = *i;
  compilearg.push_back(temp);
  return;
}

void compiler::FoundHelp(win32fe::LI &i) {
  compiler::doit = NULL;
  tool::FoundHelp(i);
  return;
}

void compiler::FoundVersion(win32fe::LI &i) {
  compiler::doit = NULL;
  tool::FoundVersion(i);
  return;
}

void compiler::FixOutput(void) {
  if (OutputFlag!=compilearg.end()) {
    std::string outfile = *OutputFlag;
    compilearg.erase(OutputFlag);
    if (compiler::doit == &compiler::Compile) {
      outfile = compileoutflag + outfile;
      compilearg.push_back(outfile);
      OutputFlag = --compilearg.end();
    } else {
      outfile = linkoutflag + outfile;
      linkarg.push_front(outfile);
      OutputFlag = --linkarg.end();
    }
  }
  return;
}

int compiler::DisplayVersion(void) {
  tool::DisplayVersion();
  std::string version = compilearg.front();

  win32fe::OutputPipe out(verbose);
  bool erc = out.Start(version.c_str());
  if (erc) {
    out.GetExitCode();
    out.OutputLine();
  } else {
    /* Should there be an error message here? */
    return(1);
  }
  return(0);
}

bool compiler::IsAKnownTool(void) {
  return(true);
}
