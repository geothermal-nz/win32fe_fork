#include "outpipe_i.h"
#include <sstream>

using namespace win32fe;

OutputPipe::OutputPipe(bool Verbose) {
  verbose       = Verbose;
  outfilename   = win32fe::GetUniqueTempFileName(verbose);
  outfile       = win32fe::CreateFile(outfilename,verbose);
  ::ZeroMemory(&si,sizeof(STARTUPINFO));
  si.cb         = sizeof(STARTUPINFO);
  si.dwFlags    = STARTF_USESTDHANDLES;
  si.hStdInput  = win32fe::GetStdIOHandle(STD_INPUT_HANDLE,verbose);
  si.hStdOutput = outfile;
  si.hStdError  = outfile;
  
  ::ZeroMemory(&pi,sizeof(PROCESS_INFORMATION));
}

OutputPipe::~OutputPipe() {
  win32fe::CloseHandle(pi.hProcess);
  win32fe::CloseHandle(outfile);
  win32fe::DeleteFile(outfilename,verbose);
}

DWORD OutputPipe::Execute(const char *str) {
  Start(str);
  return(GetExitCode());
}

bool OutputPipe::Start(const char *str) {
  bool erc = win32fe::CreateProcess(const_cast<char *>(str),&si,&pi,verbose);
  return(erc);
}

DWORD OutputPipe::GetExitCode(void) {
  DWORD exit_code;
  if (pi.hProcess!=win32fe::INVALID_HANDLE) {
    ::WaitForSingleObject(pi.hProcess,INFINITE);
    ::GetExitCodeProcess(pi.hProcess,&exit_code);
  }
  win32fe::SetFilePointer(outfile,(LONG)0,verbose);
  return(exit_code);
}

std::string OutputPipe::GetLine(void) {
  std::ostringstream line;
  DWORD fileoffset=win32fe::GetFilePointer(outfile,verbose);
  for (;;) {
    int linefeeds=0;
    std::string tmp=win32fe::ReadFile(outfile,80,verbose);
    if (tmp.length()==0) break;
    std::string::size_type n = tmp.find("\n");
    if (n==std::string::npos) {
      /* Line is incomplete. */
      line << tmp;
      fileoffset = win32fe::GetFilePointer(outfile,verbose);
    } else {
      for(;;) {
        if (n && tmp[n-1]=='\r') {
          linefeeds++;
          n=n-1;
        } else {
          break;
        }
      }
      line << tmp.substr(0,n);
      line << "\n";
      fileoffset += n+1+linefeeds;
      win32fe::SetFilePointer(outfile,fileoffset,verbose);
      break;
    }
  }
  return(line.str());
}

void OutputPipe::OutputLine(void) {
  std::cout << GetLine();
  return;
}

void OutputPipe::OutputAll(void) {
  for (;;) {
    std::string line=GetLine();
    if (line.length()==0) break;
    std::cout << line;
  }
  return;
}
