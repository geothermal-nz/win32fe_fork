#include <Windows.h>
#include <Shlwapi.h>
#include <cstring>
#include "utils_i.h"

const HANDLE win32fe::INVALID_HANDLE=INVALID_HANDLE_VALUE;

#if _MSC_VER < 1300
const DWORD  win32fe::INVALID_FILE_OFFSET=INVALID_FILE_SIZE;
#else
const DWORD  win32fe::INVALID_FILE_OFFSET=INVALID_SET_FILE_POINTER;
#endif

void win32fe::Merge(std::string &str,std::string::size_type maxlen,win32fe::List &liststr,win32fe::LI &i) {
  std::string::size_type len=str.length();
  while (i!=liststr.end()) {
    std::string trystr=*i;
    std::string::size_type space,quote;
    space=trystr.find(" ");
    quote=trystr.find("\"");
    if (space!=std::string::npos && quote==std::string::npos) {
      trystr = "\"" + trystr + "\"";
    }
    if ((maxlen==std::string::npos)||((len+trystr.length()+1) < maxlen)) {
      str += " " + trystr;
      len = str.length();
      i++;
    } else {
      break;
    }
  }
}

void win32fe::ProtectQuotes(std::string &name) {
  std::string::size_type a,b;
  a = name.find("\"");
  if (a!=std::string::npos) {
    std::string temp = name.substr(0,a+1);
    temp += "\\\"";
    temp += name.substr(a+1,std::string::npos);
    name = temp;
    b = name.rfind("\"");
    if (b!=a+2) {
      temp = name.substr(0,b);
      temp += "\\\"";
      temp += name.substr(b,std::string::npos);
      name = temp;
    }
  }
}

void win32fe::RemoveQuotes(std::string &name) {
  if ((name[0]=='\'') || (name[0]=='\"')) {
    name=name.substr(1);
  }
  std::string::size_type n=name.length();
  if ((name[n-1]=='\'') || (name[n-1]=='\"')) {
    name=name.substr(0,n-1);
  }
}

std::string win32fe::UpperCaseString(const std::string a) {
  std::string::size_type n = a.length();
  std::string A=a;
  for (int i=0;i<n;i++) {
    A[i] = (char)toupper((int)A[i]);
  }
  return(A);
}

void win32fe::PrintListString(win32fe::List &liststr) {
  std::cout << "Printing..." << std::endl;
  win32fe::LI i = liststr.begin();
  while (i!=liststr.end()) std::cout << *i++ << " ";
  std::cout << std::endl;
}

std::string win32fe::GetStringValueFromRegistry(HKEY hk,const std::string strkey,const std::string strvalue,bool verbose) {
  std::string hkeystr;
  if (hk == HKEY_LOCAL_MACHINE) {
    hkeystr = "HKEY_LOCAL_MACHINE";
  } else if (hk == HKEY_CURRENT_USER) {
    hkeystr = "HKEY_CURRENT_USER";
  } else if (hk == HKEY_CLASSES_ROOT) {
    hkeystr = "HKEY_CLASSES_ROOT";
  } else if (hk == HKEY_CURRENT_CONFIG) {
    hkeystr = "HKEY_CURRENT_CONFIG";
  } else if (hk == HKEY_CURRENT_USER) {
    hkeystr = "HKEY_CURRENT_USER";
  } else if (hk == HKEY_LOCAL_MACHINE) {
    hkeystr = "HKEY_LOCAL_MACHINE";
  } else if (hk == HKEY_USERS) {
    hkeystr = "HKEY_USERS";
  } else if (hk == HKEY_PERFORMANCE_DATA) {
    hkeystr = "HKEY_PERFORMANCE_DATA";
  } else if (hk == HKEY_DYN_DATA) {
    hkeystr = "HKEY_DYN_DATA";
  }
  std::string value="";
  HKEY key;
  /* First open key with query access. */
  long ierr = ::RegOpenKeyEx(hk,strkey.c_str(),NULL,KEY_QUERY_VALUE,&key);
  if (ierr==ERROR_SUCCESS) {
    /* Query value of key */
    unsigned long length = 256*sizeof(unsigned char);
    unsigned char buff[256];
    buff[255]='\0';
    ierr = ::RegQueryValueEx(key,strvalue.c_str(),NULL,NULL,buff,&length);
    if (ierr==ERROR_SUCCESS) {
      /* Copy char * into std::string */ 
      value = (std::string)((char *)buff);
    } else if (ierr==ERROR_MORE_DATA) {
      /* Handle buffer too small error. */
      unsigned char *newbuff = (unsigned char *)malloc(length+1);
      ierr = ::RegQueryValueEx(key,strvalue.c_str(),NULL,NULL,newbuff,&length);
      if (ierr==ERROR_SUCCESS) {
        /* Copy char * into std::string */
        value = (std::string)((char *)newbuff);
      } else {
        /* Uh oh.  This should never occur. */
        if (verbose) {
          std::cout << "win32fe::GetStringValueFromRegistry RegQueryValueEx failed on second pass." << std::endl;
          std::cout << win32fe::GetLastErrorMessage() << std::endl;
        }
      }
      /* Don't forget to free the new buffer regardless of success on second query pass. */
      free(newbuff);
    } else {
      /* Don't know how to handle any other error, so report it if verbose. */
      if (verbose) {
        std::cout << "win32fe: Attempted to Get: " << hkeystr << "\\" << strkey << ": " << strvalue << std::endl;
        std::cout << "win32fe::GetStringValueFromRegistry RegQueryValueEx failed." << std::endl;
        std::cout << win32fe::GetLastErrorMessage() << std::endl;
      }
    }
    /* Regardless of query success, close opened key. */
    ::RegCloseKey(key);
  } else {
    /* Failed to open key. */
    if (verbose) {
      std::cout << "win32fe: Attempted to Open: " << hkeystr << "\\" << strkey << std::endl;
      std::cout << "win32fe::GetStringValueFromRegistry RegOpenKeyEx failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(value);
}

extern "C" __declspec(dllexport) size_t win32fe_GetShortPath(const char *path,const size_t buff_size,char *shortpath,int verbose);
std::string::size_type win32fe::GetShortPath(std::string &path,bool verbose) {
  int Verbose;
  if (verbose) {
    Verbose=-1;
  } else {
    Verbose=0;
  }
  char buff[MAX_PATH];
  std::string::size_type buffsize = ::win32fe_GetShortPath(path.c_str(),MAX_PATH,buff,Verbose);
  if (buffsize>0 && buffsize<=MAX_PATH) {
    path = (std::string)buff;
  }
  return(buffsize);
}

extern "C" __declspec(dllexport) size_t win32fe_GetLongPath(const char *path,const size_t buff_size,char *longpath,int verbose);
std::string win32fe::GetLongPath(const std::string shortpath,bool verbose) {
  int Verbose;
  if (verbose) {
    Verbose=-1;
  } else {
    Verbose=0;
  }
  char buff[MAX_PATH];
  std::string::size_type buffsize = ::win32fe_GetLongPath(shortpath.c_str(),MAX_PATH,buff,Verbose);
  if (buffsize>0 && buffsize<=MAX_PATH) {
    return((std::string)buff);
  }
  return("");
}

bool win32fe::GetPathRelative(std::string &path,const std::string &reltodir,bool verbose) {
  char buff[MAX_PATH];
  bool success = ::PathRelativePathTo(buff,reltodir.c_str(),
                                      FILE_ATTRIBUTE_DIRECTORY,
                                      path.c_str(),
                                      FILE_ATTRIBUTE_NORMAL); /* Shlwapi.h */
  if (success) {
    path = (std::string)buff;
  } else {
    if (verbose) {
      std::cout << "win32fe::GetPathRelative PathRelativePathTo failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(success);
}

std::string win32fe::GetCurrentDirectory(bool verbose) {
  std::string str;
  char buff[MAX_PATH+1];
  buff[MAX_PATH]=buff[0]='\0';
  char *buffer=buff;

  DWORD success = ::GetCurrentDirectory(MAX_PATH,buffer);
  if (!success) {
    if (verbose) {
      std::cout << "win32feutils::GetCurrentDirectory failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  if (success<=MAX_PATH) {
    buffer[success]='\0';
    str = (std::string)buffer;
  } else {
    buffer = new(char[success+1]);
    buffer[0]='\0';
    success = ::GetCurrentDirectory(success,buffer);
    if (!success) {
      if (verbose) {
        std::cout << "win32feutils::GetCurrentDirectory failed on second call." << std::endl;
        std::cout << win32fe::GetLastErrorMessage() << std::endl;
      }
    }
    buffer[success]='\0';
    str = (std::string)buffer;
    delete(buffer);
  }
  return(str);
}

std::string win32fe::SearchPath(const std::string file,const std::string extension,bool verbose) {
  /*
    Note: There are 6 locations searched, the 6th of these is the PATH variable.
    But, in reality, the first 5 don't make any sense for win32fe tools, so it is 
    assumed that the tool was found in the PATH variable.
  */
  std::string str;
  char buff[MAX_PATH+1];
  buff[MAX_PATH]=buff[0]='\0';
  char *buffer=buff,*tmp;

  DWORD success = ::SearchPath(NULL,file.c_str(),extension.c_str(),MAX_PATH,buffer,&tmp);
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::SearchPath SearchPath failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  } 
  if (success<=MAX_PATH) {
    buffer[success]='\0';
    str = (std::string)buffer;
  } else {
    buffer = new(char[success+1]);
    buffer[0]='\0';
    success = ::SearchPath(NULL,file.c_str(),extension.c_str(),success,buffer,&tmp);
    if (!success) {
      if (verbose) {
        std::cout << "win32fe::SearchPath SearchPath failed on second call." << std::endl;
        std::cout << win32fe::GetLastErrorMessage() << std::endl;
      }
    }
    buffer[success]='\0';
    str=buffer;
    delete(buffer);
  }
  return(str);
}

std::string win32fe::GetUniqueTempFileName(bool verbose) {
  TCHAR file[MAX_PATH];
  DWORD success = ::GetTempPath(MAX_PATH,file);
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::GetUniqueTempFileName GetTempPath failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
    return("");
  }
  UINT ok = ::GetTempFileName(file,"wfe",0,file);
  if (!ok) {
    if (verbose) {
      std::cout << "win32fe::GetUniqueTempFileName GetTempFileName failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
    return("");
  }
  return((std::string)file);
}

HANDLE win32fe::GetUniqueTempFile(bool verbose) {
  std::string filename = win32fe::GetUniqueTempFileName(verbose);
  if (filename.length()==0) {
    return(INVALID_HANDLE_VALUE);
  }
  SECURITY_ATTRIBUTES sa;
  sa.nLength=sizeof(SECURITY_ATTRIBUTES);
  sa.lpSecurityDescriptor=NULL;
  sa.bInheritHandle=TRUE;
  HANDLE h = ::CreateFile(filename.c_str(),
                          GENERIC_READ | GENERIC_WRITE | DELETE | STANDARD_RIGHTS_ALL,
                          FILE_SHARE_READ | FILE_SHARE_WRITE,
                          &sa,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
  if (h==INVALID_HANDLE_VALUE) {
    if (verbose) {
      std::cout << "win32fe::GetUniqueTempFile CreateFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return (h);
}

std::string win32fe::GetTempDir(bool verbose) {
  char path[MAX_PATH];

  int success = ::GetTempPath(MAX_PATH,path);
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::GetTempPath GetTempPath failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return ((std::string)path);
}

HANDLE win32fe::CreateFile(std::string filename,bool verbose) {
  if (verbose) {
    std::cout << "Attempting to create file: " << filename << std::endl;
  }
  if (filename.length()==0) {
    if (verbose) {
      std::cout << "win32fe::CreateFile failed." << std::endl;
      std::cout << "No filename specified." << std::endl;
    }
    return(INVALID_HANDLE_VALUE);
  }

  SECURITY_ATTRIBUTES sa;
  sa.nLength=sizeof(SECURITY_ATTRIBUTES);
  sa.lpSecurityDescriptor=NULL;
  sa.bInheritHandle=TRUE;
  HANDLE h = ::CreateFile(filename.c_str(),
                          GENERIC_READ | GENERIC_WRITE | DELETE | STANDARD_RIGHTS_ALL,
                          FILE_SHARE_READ | FILE_SHARE_WRITE,
                          &sa,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);

  if (h==INVALID_HANDLE_VALUE) {
    if (verbose) {
      std::cout << "win32fe::CreateFile CreateFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return (h);
}

std::string win32fe::ReadFile(HANDLE read,const DWORD request,bool verbose) {
  char *buff=new(char[request+1]);
  DWORD numread;
  std::string buffer;
  BOOL success = ::ReadFile(read,buff,request,&numread,NULL);
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::ReadFile ReadFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
    buffer="";
  } else {
    buff[numread]='\0';
    buffer=(std::string)buff;
  }
  delete(buff);
  return(buffer);
}

DWORD win32fe::WriteFile(HANDLE write,std::string buffer,bool verbose) {
  DWORD numwritten;
  BOOL success = ::WriteFile(write,buffer.c_str(),buffer.length(),&numwritten,NULL);
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::WriteFile WriteFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  } else if (numwritten!=buffer.length()) {
    if (verbose) {
      std::cout << "win32fe::WriteFile WriteFile incomplete." << std::endl;
    }
  }
  return(numwritten);
}

bool win32fe::DeleteFile(std::string filename,bool verbose) {
  if (verbose) {
    std::cout << "del " << filename << std::endl;
  }
  bool success = ::DeleteFile(filename.c_str());
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::DeleteFile DeleteFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(success);
}
  
DWORD win32fe::SetFilePointer(HANDLE file,LONG offset,bool verbose) {
  DWORD fp = ::SetFilePointer(file,offset,NULL,FILE_BEGIN);
  if (fp==win32fe::INVALID_FILE_OFFSET && ::GetLastError()!=NO_ERROR) {
    if (verbose) {
      std::cout << "win32fe::SetFilePointer SetFilePointer failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(fp);
}

DWORD win32fe::GetFilePointer(HANDLE file,bool verbose) {
  DWORD fp = ::SetFilePointer(file,(LONG)0,NULL,FILE_CURRENT);
  if (fp==win32fe::INVALID_FILE_OFFSET) {
    if (verbose) {
      std::cout << "win32fe::GetFilePointer SetFilePointer failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(fp);
}

bool win32fe::CopyFile(std::string src,std::string dest,bool verbose) {
  if (verbose) {
    std::cout << "win32fe::CopyFile copying " << src << " to " << dest << std::endl;
  }
  bool success = ::CopyFile(src.c_str(),dest.c_str(),FALSE);
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::CopyFile CopyFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(success);
}

bool win32fe::MoveFile(std::string src,std::string dest,bool verbose) {
  if (verbose) {
    std::cout << "win32fe::MoveFile moving " << src << " to " << dest << std::endl;
  }
  bool success = ::MoveFile(src.c_str(),dest.c_str());
  if (!success) {
    if (verbose) {
      std::cout << "win32fe::MoveFile MoveFile failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(success);
}

std::string win32fe::GetEnvironmentVariable(const std::string variable,bool verbose) {
  std::string str;
  DWORD bufflen = 1024;
  char buff[1024+1];
  buff[1024]=buff[0]='\0';
  char *buffer=buff;

  DWORD success = ::GetEnvironmentVariable(variable.c_str(),buffer,bufflen);
  if (!success) {
    if (verbose) {
      std::cout << "win32feutils::GetEnvironmentVariable failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  if (success<=bufflen) {
    buffer[success]='\0';
    str = (std::string)buffer;
  } else {
    buffer = new(char[success+1]);
    buffer[0]='\0';
    success = ::GetEnvironmentVariable(variable.c_str(),buffer,success);
    if (!success) {
      if (verbose) {
        std::cout << "win32feutils::GetEnvironmentVariable failed on second call." << std::endl;
        std::cout << win32fe::GetLastErrorMessage() << std::endl;
      }
    }
    buffer[success]='\0';
    str = (std::string)buffer;
    delete(buffer);
  }
  return(str);
}

bool win32fe::SetEnvironmentVariable(const std::string variable,const std::string value,bool verbose) {
  bool success=::SetEnvironmentVariable(variable.c_str(),value.c_str());
  if (!success) {
    if (verbose) {
      std::cout << "win32feutils::SetEnvironmentVariable failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(success);
}

extern "C" __declspec(dllexport) char * win32fe_GetLastErrorMessage(void);
std::string win32fe::GetLastErrorMessage(void) {
  char *buff=::win32fe_GetLastErrorMessage();
  std::string msg=buff;
  LocalFree(buff);
  return(msg);
}

bool win32fe::CreateProcess(const std::string str,STARTUPINFO *si,PROCESS_INFORMATION *pi,bool verbose) {
  if (verbose) {
    std::cout << str << std::endl;
  }
  BOOL created = ::CreateProcess(NULL,const_cast<char *>(str.c_str()),NULL,NULL,TRUE,0,NULL,NULL,si,pi);
  if (!created) {
    if (verbose) {
      std::cout << "win32feutils::CreateProcess failed." << std::endl;
      std::cout << str << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(bool(created));
}

bool win32fe::CreatePipe(PHANDLE read,PHANDLE write,LPSECURITY_ATTRIBUTES pipe_sa,bool verbose) {
  BOOL created = ::CreatePipe(read,write,pipe_sa,0);
  if (!created) {
    if (verbose) {
      std::cout << "win32feutils::CreatePipe failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return((bool)created);
}

bool win32fe::SetStdIOHandle(DWORD stdiohandle,HANDLE h,bool verbose) {
  BOOL handleset = ::SetStdHandle(stdiohandle,h);
  if (!handleset) {
    if (verbose) {
      std::cout << "win32feutils::SetStdIOHandle ";
      switch(stdiohandle) {
      case STD_OUTPUT_HANDLE: {
        std::cout << "STD_OUTPUT_HANDLE";
        break;
      }
      case STD_ERROR_HANDLE: {
        std::cout << "STD_ERROR_HANDLE";
        break;
      }
      case STD_INPUT_HANDLE: {
        std::cout << "STD_INPUT_HANDLE";
        break;
      }
      default: {
        std::cout << "UNKNOWN";
        break;
      }
      }
      std::cout << " failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return((bool)handleset);
}

HANDLE win32fe::GetStdIOHandle(DWORD stdiohandle,bool verbose) {
  HANDLE h = ::GetStdHandle(stdiohandle);
  if (h==win32fe::INVALID_HANDLE) {
    if (verbose) {
      std::cout << "win32feutils::GetStdIOHandle ";
      switch(stdiohandle) {
      case STD_OUTPUT_HANDLE: {
        std::cout << "STD_OUTPUT_HANDLE";
        break;
      }
      case STD_ERROR_HANDLE: {
        std::cout << "STD_ERROR_HANDLE";
        break;
      }
      case STD_INPUT_HANDLE: {
        std::cout << "STD_INPUT_HANDLE";
        break;
      }
      default: {
        std::cout << "UNKNOWN";
        break;
      }
      }
      std::cout << " failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return(h);
}

bool win32fe::DuplicateLocalHandle(HANDLE in,LPHANDLE out,DWORD access,bool inheritable,DWORD options,bool verbose) {
  BOOL dupd = ::DuplicateHandle(::GetCurrentProcess(),in,
                                ::GetCurrentProcess(),out,
                                access,(BOOL)inheritable,options);
  if (!dupd) {
    if (verbose) {
      std::cout << "win32fe::DuplicateLocalHandle failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return((bool)dupd);
}

bool win32fe::CloseHandle(HANDLE in,bool verbose) {
  BOOL closed = ::CloseHandle(in);
  if (!closed) {
    if (verbose) {
      std::cout << "win32fe::CloseHandle failed." << std::endl;
      std::cout << win32fe::GetLastErrorMessage() << std::endl;
    }
  }
  return((bool)closed);
}
