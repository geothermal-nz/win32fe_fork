#include <stdlib.h>
#include "dffe.h"
#include "outpipe.h"
#include "utils.h"

using namespace win32fe;

df::df() {
  compileoutflag = "/object:";
  linkoutflag    = "/exe:";

  output_name    = "";
  VisualCDir     = "";
  cygroot        = "";
}

int df::Help(void) {
  compiler::Help();
  std::cout << "df specific help:" << std::endl;
  std::cout << "  For mixed language C/FORTRAN programming in PETSc, the location of the" << std::endl;
  std::cout << "      C system libraries must also be specified in the LIB environment" << std::endl;
  std::cout << "      variable or with -L<dir> since the installed location of these" << std::endl;
  std::cout << "      libraries may be independent of the FORTRAN installation." << std::endl;
  std::cout << "      If installed, the Visual Studio 6, C system libraries are" << std::endl;
  std::cout << "      automatically found and added to the path." << std::endl;
  std::cout << "  -g is equal to -debug:full" << std::endl;
  std::cout << "  -O is equal to -optimize:3" << std::endl << std::endl;
  std::cout << "=========================================================================" << std::endl << std::endl;
  std::string help = compilearg.front();
  help += " -?";

  int ierr = tool::Report(help.c_str());
  return(ierr);
}

int df::DisplayVersion(void) {
  tool::DisplayVersion();
  std::string version = compilearg.front();

  win32fe::OutputPipe out(verbose);
  if (out.Start(version.c_str())) {
    out.GetExitCode();
    out.OutputLine();
  } else {
    /* Should there be an error message here? */
    return(1);
  }
  return(0);
}

int df::Report(const char *str) {
  return(compiler::Report(str));
}

void df::FoundI(win32fe::LI &i) {
  /* std::string shortpath=win32fe::UpperCaseString(i->substr(2)); */
  std::string shortpath=i->substr(2); /* uppercase messes up -I MPICH_INC for Compaq f90 compiler.. */
  if (win32fe::GetShortPath(shortpath)) {
    shortpath = "-include:"+shortpath;
  } else {
    shortpath = *i;
  }
  compilearg.push_back(shortpath);
  return;
}

void df::FoundO(win32fe::LI &i) {
  std::string temp=*i;
  if (temp=="-O") {
    compilearg.push_back("-optimize:3");
  } else {
    compilearg.push_back(temp);
  }
  return;
}

void df::Foundg(win32fe::LI &i) {
  std::string temp=*i;
  if (temp=="-g") {
    compilearg.push_back("-debug:full");
  } else {
    compilearg.push_back(temp);
  }
  return;
}

void df::Foundl(win32fe::LI &i) {
  std::string temp=*i;
  std::string::size_type n=temp.find("-libs:");
  if (n != std::string::npos) {
    compilearg.push_back(temp);
  } else {
    cl::Foundl(i);
  }
  return;
}

void df::Foundm(LI &i) {
  std::string shortpath = i->substr(8);
  if (win32fe::GetShortPath(shortpath)) {
    shortpath = "-module:"+shortpath;
    compilearg.push_back(shortpath);
  } else {
    if (!woff) {
      std::cout << "Warning: df: module Path Not Found: " << i->substr(8) << std::endl;
    }
  }
  return;
}

void df::FoundD(win32fe::LI &i) {
  std::string temp=*i;
  bool dllflag=false;
  if (temp.length()>2 && (temp[2]=='l' || temp[2]=='L')) {
    if (temp.length()==3) {
      dllflag = true;
    } else if (temp[3]=='l' || temp[3]=='L') {
      if (temp.length()==4) {
        dllflag = true;
      } else if (temp.length()>5 && temp[4]==':') {
        dllflag = true;
      }
    }
  }
  if (dllflag) {
    linkoutflag="/dll:";
  } else if (temp[1]=='D') {
    compiler::FoundD(i);
  } else {
    compiler::FoundUnknown(i);
  }
  return;
}

void df::FoundUnknown(win32fe::LI &i) {
  std::string temp=*i;
  if (temp[1]=='d') {
    FoundD(i);
  } else {
    compiler::FoundUnknown(i);
  }
  return;
}

void df::FixOutput(void) {
  if (doit == &compiler::Link) {
    std::string tmpdir = win32fe::GetTempDir();
    if (tmpdir.rfind("\\")!=tmpdir.length()-1) {
      tmpdir += "\\";
    }
    std::string outfile="";
    if (OutputFlag==compilearg.end()) {
      /* No output file was specified, so build an output name based on the first input file */
      std::string ext=".exe";
      if (linkoutflag == "/dll:") {
        ext = ".dll";
      }
      std::string cwd=win32fe::GetCurrentDirectory();
      if (cwd.rfind("\\")!=cwd.length()-1) {
        cwd += "\\";
      }
      outfile=file.front();
      std::string::size_type a=outfile.rfind("\\");
      if (a==std::string::npos) {
        a=outfile.rfind("/");
      }
      if (a==std::string::npos) {
        a=0;
      } else {
        a+=1;
      }
      outfile=outfile.substr(a);
      std::string::size_type b=outfile.rfind(".");
      output_name = cwd    + outfile.substr(0,b)+ext;
      outfile     = tmpdir + outfile.substr(0,b)+ext;
      std::cout << "output_name: " + output_name << std::endl << "outfile: " + outfile <<std::endl;
    } else {
      std::string outname=*OutputFlag;
      std::string::size_type n = outname.rfind("\\")+1;
      output_name = outname;
      if (output_name.substr(n).rfind(".")==std::string::npos) {
        if (linkoutflag == "/dll:") {
          output_name = output_name + ".dll";
        } else {
          output_name = output_name + ".exe";
        }
      }
      
      compilearg.erase(OutputFlag);
      outfile = tmpdir + outfile;
    }
    arg.push_back("-o");
    LI li = arg.end();
    li --;
    arg.push_back(outfile);
    Foundo(li);
  }
  compiler::FixOutput();
}

int df::Link(void) {
  int ierr = cl::Link();
  std::string tmpdir = win32fe::GetTempDir();
  if (tmpdir.rfind("\\")!=tmpdir.length()-1) {
    tmpdir += "\\";
  }
  std::string::size_type b = output_name.rfind("\\");
  if (b==std::string::npos) {
    b = output_name.rfind("/");
  }
  if (b==std::string::npos) {
    b=0;
  } else {
    b+=1;
  }
  std::string ext=".exe";
  if (linkoutflag == "/dll:") {
    ext=".dll";
  }
  std::string basename = output_name.substr(b);
  std::string::size_type n=basename.rfind(".");
  if (n==std::string::npos) {
    basename = basename+ext;
  }
  std::string outfile = tmpdir + basename;
  if (output_name != outfile) {
    if (!win32fe::MoveFile(outfile,output_name,verbose)) {
      win32fe::DeleteFile(output_name,verbose);
      win32fe::MoveFile(outfile,output_name,verbose);
    }
  }
  return(ierr);
}
