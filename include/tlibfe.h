#if !defined(win32fe_TlibFE_h)
#define win32fe_TlibFE_h

#include "archiverfe.h"

namespace win32fe {

  class tlib : public archiver {
  public:
    tlib();
    ~tlib() {}
    /* protected: */
    virtual int Archive(void);
    virtual int List(void);
    virtual int Delete(void);
    virtual int Extract(void);
    virtual int Help(void);

    virtual int Report(const char *);

    virtual void FoundFile(win32fe::LI &);
    virtual void FoundFlag(win32fe::LI &);

  };

}

#endif
