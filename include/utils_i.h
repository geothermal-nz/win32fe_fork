#if !defined(win32feutils_Utils_i_h_)
#define win32feutils_Utils_i_h_

#define WIN32FE_UTILS_DLL_NAME "win32feutils.dll"

#if !defined(win32feutils_user)
#  define API_CALL __declspec (dllexport)
#  define API_DECL __declspec (dllexport) extern
#else
#  define API_CALL __declspec (dllimport)
#  define API_DECL __declspec (dllimport) extern
#endif

#if defined(__cplusplus)

#include "fe.h"
#include <Windows.h>

namespace win32fe {

  API_CALL void Merge(std::string &,std::string::size_type,win32fe::List &,win32fe::LI &);
  API_CALL void RemoveQuotes(std::string &);
  API_CALL void ProtectQuotes(std::string &);
  API_CALL void PrintListString(std::list<std::string> &);
  API_CALL std::string UpperCaseString(const std::string);
  API_CALL std::string GetStringValueFromRegistry(HKEY,const std::string,const std::string,bool verbose=false);
  API_CALL std::string::size_type GetShortPath(std::string &path,bool verbose=false);
  API_CALL std::string GetLongPath(const std::string shortpath,bool verbose=false);
  API_CALL bool        GetPathRelative(std::string &path,const std::string &reltodir,bool verbose=false);
  API_CALL std::string GetCurrentDirectory(bool verbose=false);
  API_CALL std::string SearchPath(const std::string file,const std::string extension,bool verbose=false);
  API_CALL std::string GetUniqueTempFileName(bool verbose=false);
  API_CALL HANDLE      GetUniqueTempFile(bool verbose=false);
  API_CALL std::string GetTempDir(bool verbose=false);
  API_CALL HANDLE      CreateFile(std::string filename,bool verbose=false);
  API_CALL std::string ReadFile(HANDLE write,const DWORD request,bool verbose=false);
  API_CALL DWORD       WriteFile(HANDLE write,std::string buff,bool verbose=false);
  API_CALL bool        DeleteFile(std::string,bool verbose=false);
  API_CALL DWORD       SetFilePointer(HANDLE file,LONG offset,bool verbose=false);
  API_CALL DWORD       GetFilePointer(HANDLE file,bool verbose=false);
  API_CALL bool        CopyFile(std::string,std::string,bool verbose=false);
  API_CALL bool        MoveFile(std::string,std::string,bool verbose=false);
  API_CALL std::string GetEnvironmentVariable(const std::string variable,bool verbose=false);
  API_CALL bool        SetEnvironmentVariable(const std::string variable,const std::string value,bool verbose=false);
  API_CALL std::string GetLastErrorMessage(void);

  API_CALL bool        CreateProcess(const std::string str,STARTUPINFO *si,
                                     PROCESS_INFORMATION *pi,bool verbose=false);
  API_CALL bool        CreatePipe(PHANDLE read,PHANDLE write,LPSECURITY_ATTRIBUTES pipe_sa,bool verbose=false);
  API_CALL bool        SetStdIOHandle(DWORD stdiohandle,HANDLE h,bool verbose=false);
  API_CALL HANDLE      GetStdIOHandle(DWORD stdiohandle,bool verbose=false);
  API_CALL bool        DuplicateLocalHandle(HANDLE in,LPHANDLE out,
                                            DWORD access,bool inheritable,DWORD options,bool verbose=false);
  API_CALL bool        CloseHandle(HANDLE in,bool verbose=false);
  API_DECL const HANDLE INVALID_HANDLE;
  API_DECL const DWORD  INVALID_FILE_OFFSET;

}

#endif

#endif
