#if !defined(win32fe_FE_h_)
#define win32fe_FE_h_

#include <list>
#include <string>
#include <map>
#include <iostream>

namespace win32fe {

  typedef std::list<std::string> List;
  typedef win32fe::List::iterator LI;

}
#endif
