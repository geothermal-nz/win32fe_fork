#if !defined(win32feutils_OutPipe_i_h_)
#define win32feutils_OutPipe_i_h_

#include <string>
#include <Windows.h>
#include "utils_i.h"

namespace win32fe {

  class OutputPipe {
    HANDLE              outfile;
    STARTUPINFO         si;
    PROCESS_INFORMATION pi;
    bool                verbose;
    std::string         outfilename;
  public:
    API_CALL OutputPipe(bool);
    API_CALL ~OutputPipe();
    API_CALL DWORD Execute(const char *);
    API_CALL bool  Start  (const char *);
    API_CALL DWORD GetExitCode(void);
    API_CALL std::string GetLine(void);
    API_CALL void OutputLine(void);
    API_CALL void OutputAll(void);
  };

}
#endif
