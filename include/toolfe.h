#if !defined(win32fe_ToolFE_h_)
#define win32fe_ToolFE_h_

#include "fe.h"

namespace win32fe {

  class tool {
  public:
    static tool* Create(int argc,char *argv[]);
    virtual int Parse(void);
    virtual int Execute(void);
    virtual void Destroy(void) {delete this;}
    /* protected: */
    tool();
    virtual ~tool();
    void GetArgs(int argc,char *argv[]);

    virtual int Help(void);
    virtual int DisplayVersion(void);

    virtual int Report(const char *);

    virtual void FoundHelp(win32fe::LI &);
    virtual void FoundVersion(win32fe::LI &);
    virtual void FoundVerbose(win32fe::LI &);
    void FoundPath(win32fe::LI &);
    void FoundUse(win32fe::LI &);
    void FoundWoff(win32fe::LI &);
    void FoundNT4(win32fe::LI &);
    void FoundWait(win32fe::LI &);
    void FoundWin_l(win32fe::LI &);

    void FoundUnknown(win32fe::LI &);

    virtual void FoundFile(win32fe::LI &);
    virtual bool IsAKnownTool(void);

    std::list<std::string> arg;
    std::list<std::string> file;
    std::string InstallDir;
    std::string version_string;
    bool autodetect;
    bool inpath;
    bool verbose;
    bool woff;
    std::string libprefix;
    int waitfordebugger;
  private:
    typedef void (win32fe::tool::*ptm_parse)(win32fe::LI &);
    std::map<std::string,ptm_parse> Options;
    typedef int (win32fe::tool::*ptm_execute)(void);
    ptm_execute doit;
    void SetVersion(void);
  };

}
#endif
