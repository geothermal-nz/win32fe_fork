#if !defined(win32fe_BccFE_h)
#define win32fe_BccFE_h

#include "compilerfe.h"

namespace win32fe {

  class bcc : public compiler {
  public:
    bcc();
    ~bcc() {}
    virtual int Parse(void);
    /* protected: */
    virtual int Compile(void);
    virtual int Link(void);
    virtual int Help(void);

    virtual void Foundl(win32fe::LI &);
    virtual void Foundg(win32fe::LI &);

    virtual std::string BuildLinkCommand(void);
  };

}

#endif
