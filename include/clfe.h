#if !defined(win32fe_ClFE_h)
#define win32fe_ClFE_h

#include "compilerfe.h"

namespace win32fe {

  class cl : public compiler {
  public:
    cl();
    ~cl() {}
    virtual int Parse(void);
    /* protected: */
    virtual int Compile(void);
    virtual int Help(void);
    virtual int CLHelp(void);
    virtual int DisplayVersion(void);

    virtual int Report(const char *);

    virtual void FoundL(win32fe::LI &);
    virtual void Foundl(win32fe::LI &);
    virtual void Foundg(win32fe::LI &);
    virtual void FoundO(win32fe::LI &);

    virtual void FoundUnknown(win32fe::LI &);

    bool        logo;
    std::string VisualStudioDir;
    std::string VSVersion;
  };

  // ---------------------------------------------------
  // NVCC CUDA NVIDIA - Arthur Besen Soprano 10/10/2011
  // ---------------------------------------------------
  //  ---------------------------
  //  NVIDIA CLASS
  //  ---------------------------
  class nvcc : public cl {
  public:
      nvcc(){
          compileoutflag = "-o ";
          linkoutflag    = "-o ";
          OutputFlag = compilearg.end();

          logo = false;
          VisualStudioDir="";
          VSVersion="";
      }
      ~nvcc() {}
      virtual int Link(void);
      virtual void FoundUnknown(win32fe::LI &);
      virtual int Parse(void) {
          //linkarg.push_front("-link");
          int ierr = compiler::Parse();
          if (ierr) {
              return(ierr);
          }
          if (!verbose && !logo) {
              compilearg.push_back("");
          }
          return(0);
      }
      virtual int DisplayVersion(void);

  };
  //  ---------------------------
  //  END OF NVIDIA CLASS
  //  ---------------------------
  // ---------------------------------------------------
  // END OF NVCC CUDA NVIDIA - Arthur Besen Soprano 10/10/2011
  // ---------------------------------------------------

}

#endif

